const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

exports.handler = async (event) => {
  const requestBody = JSON.parse(event.body)

  let response = {}
  const msg = {
    to: process.env.CONTACT_MAIL_TO,
    from: {
      name: requestBody.name,
      email: requestBody.email
    },
    template_id: process.env.SENDGRID_TEMPLATE_ID,
    dynamic_template_data: {
      title: requestBody.subject,
      message1: requestBody.message
    }
  }

  await sgMail.send(msg)
    .then((res) => {
      console.log(res)
      response = {
        statusCode: 200,
        body: 'success'
      }
    })
    .catch((e) => {
      console.log(e)
      response = {
        statusCode: 400,
        body: 'failed'
      }
    })

  return response
}
