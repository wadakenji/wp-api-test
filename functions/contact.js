const nodemailer = require('nodemailer')

exports.handler = async (event) => {
  let response = {}
  const requestBody = JSON.parse(event.body)

  const smtpOption = {
    host: process.env.SMTP_HOST,
    port: 465,
    secure: true, // SSL
    auth: {
      user: process.env.SMTP_USERNAME,
      pass: process.env.SMTP_PASSWORD
    }
  }

  const transporter = nodemailer.createTransport(smtpOption)

  const mailOptions = {
    from: process.env.SENDER_EMAIL_ADDRESS,
    to: process.env.CONTACT_MAIL_TO,
    subject: requestBody.subject,
    text: requestBody.message
  }

  await transporter.sendMail(mailOptions)
    .then((r) => {
      console.log(r)
      response = {
        statusCode: 200,
        body: 'success'
      }
    })
    .catch((e) => {
      console.log(e)
      response = {
        statusCode: 400,
        body: 'failed'
      }
    })

  return response
}
