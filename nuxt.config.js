const config = require('./const/config')
const axios = require('axios')
const WPAPI = require('wpapi')
const wp = new WPAPI({endpoint: config.WP_BASE_URL})

module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  generate: {
    //静的ファイルの動的な生成
    routes: async () => {
      const postsInfo = await axios.get(config.WP_BASE_URL + config.WP_CUSTOM_API_NAMESPACE + '/posts')
        .then(res => res.data)
        .catch(e => console.log(e))
      const authors = await wp.users().perPage(100).get()
      const categories = await wp.categories().perPage(100).get()


      // /page/_page のルーティング配列
      let pageRoutes = []
      for (let i = 1; i <= postsInfo.max_num_pages; i++) {
        pageRoutes.push({
          route: '/page/' + i,
          payload: {authors: authors, categories: categories}
        })
      }

      // /post/_id のルーティング配列
      let postRoutes = []
      for (let i = 1; i <= postsInfo.max_num_pages; i++) {
        const tmp = await axios.get(config.WP_BASE_URL + config.WP_CUSTOM_API_NAMESPACE + '/posts?paged=' + i)
          .then(res => res.data)
          .catch(e => console.log(e))
        postRoutes.push(...tmp.posts.map((post) => {
          return {
            route: '/post/' + post.id,
            payload: {post: post, authors: authors, categories: categories}
          }
        }))
      }

      // /author/_id のルーティング配列
      const authorRoutes = authors.map(author => {
        return {
          route: '/author/' + author.id,
          payload: {authors: authors, categories: categories}
        }
      })

      // /category/_id のルーティング配列
      const categoryRoutes = categories.map(category => {
        return {
          route: '/category/' + category.id,
          payload: {authors: authors, categories: categories}
        }
      })

      return [
        ...pageRoutes,
        ...postRoutes,
        ...authorRoutes,
        ...categoryRoutes
      ]
    }
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'nuxt-fontawesome',
  ],
  fontawesome: {
    component: 'fa',
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      },
    ],
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
